#ifndef TIMER_H
#define TIMER_H

#ifdef DEBUG
#define D(x) x
#else
#define D(x)
#endif

#include <functional>
#include <chrono>
#include <future>
#include <cstdio>

class Timer
{
public:
    template <class Callable, class... arguments>
    Timer(int after, bool async, Callable&& f, arguments&&... args)
    {

        std::function<typename std::result_of<Callable(arguments...)>::type()> 
                task(std::bind(std::forward<Callable>(f), std::forward<arguments>(args)...));

        if(async)
        {
            std::thread([after, task]() {
                std::this_thread::sleep_for(std::chrono::minutes(after));
                task();
            }).detach();
        }
        else
        {
            std::this_thread::sleep_for(std::chrono::minutes(after));
            task();
        }
    }
};

#endif // TIMER_H
