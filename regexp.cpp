#include "regexp.h"

Regexp::Regexp()
{
}

// Matches a failed entry in the log,
// it returns the matchign parts. Such as
// host, IP, username etc.
//
smatch Regexp::match_entry(sregex rgx, std::string line)
{
    smatch match;
    regex_search(line, match, rgx);
    return match;
}

// Match SSHD output from journalctl -f
// and returns a string.
//
std::string Regexp::match_sshd_msg(std::string line)
{
    smatch match;
    regex_search(line, match, SSHD_ENTRY);
    return match["message"];
}
std::string Regexp::match_sshd_msg(char* line)
{
    std::string new_line(line);
    return match_sshd_msg(new_line);
}

