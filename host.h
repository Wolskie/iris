#ifndef HOST_H
#define HOST_H


#ifdef DEBUG
#define D(x) x
#else
#define D(x)
#endif

#include <stdio.h>
#include <string>
#include <vector>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

using namespace boost::property_tree;

class Host
{
public:

    Host(ptree* prefs_, std::string ipaddr_);
    ~Host();

    // Stores the XML preferences
    ptree* prefs;

    int invalid_user_fail_count,
        valid_user_fail_count = 0;

    // List of all users who has come form this host
    std::vector<std::string> users;
    std::string ipaddr;

    void login_attempt(bool success, std::string username);

    // Constructor, one for the Host and the user,
    // some cases there might not be a user so then 
    // just initialize with the ipaddr;
    //

    bool has_user(std::string username);


};

#endif // HOST_H
