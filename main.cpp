#include <QCoreApplication>
#include <iostream>
#include "iris.h"
#include "timer.h"


int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    Iris* x = new Iris();
    x->main_loop();
    return a.exec();
}
