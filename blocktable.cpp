#include "blocktable.h"

BlockTable::BlockTable()
{


}

void BlockTable::check()
{
    puts("Purging block list..");
}

void BlockTable::register_host(Host host)
{
    hosts.push_back(host);
    D(std::cerr << "Registering " << host.ipaddr << std::endl);
}

bool BlockTable::is_registered(std::string ipaddr_)
{
    for(auto host : hosts)
    {
        if (host.ipaddr == ipaddr_)
        {
            return true;
        }
    }
    return false;
}
