#ifndef REGEXP_H
#define REGEXP_H

#ifdef DEBUG
#define D(x) x
#else
#define D(x)
#endif
#include <stdio.h>

#include <boost/xpressive/xpressive.hpp>
#include <boost/xpressive/regex_actions.hpp>

using namespace boost::xpressive;

class Regexp
{
public:
    Regexp();
    std::string match_sshd_msg(std::string line);
    std::string match_sshd_msg(char* line);
    
    smatch match_entry(sregex rgx, std::string line);

    /* Regex Filters for matching SSHD messages */
    const sregex SSHD_ENTRY = sregex::compile(".* (sshd.*:|\\[sshd\\]) (?P<message>.*)");
    const sregex FAIL_USER = sregex::compile("(?P<invalid>(Illegal|Invalid)) user (?P<user>.*?) .*from (?P<host>\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3})");
    const sregex FAIL_AUTH = sregex::compile("Authentication failure for (?P<user>.*) .*from (?P<host>\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3})");
    const sregex FAILED_PWD = sregex::compile("Failed (?P<method>.*) for (?P<invalid>invalid user |illegal user )?(?P<user>.*?) .*from (?P<host>\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3})");
    const sregex SUCCESS_AUTH = sregex::compile("Accepted (?P<method>.*) for (?P<user>.*?) from (?P<host>\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3})");


    const sregex FAIL_REGEX[3] = { FAIL_AUTH, FAILED_PWD, FAIL_USER };

};

#endif // REGEXP_H
