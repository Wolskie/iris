#ifndef IRIS_H
#define IRIS_H

#ifdef DEBUG
#define D(x) x
#else
#define D(x)
#endif

#include <stdio.h>
#include <iostream>
#include <string>
#include <thread>
#include <chrono>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

#include <boost/xpressive/xpressive.hpp>
#include <boost/xpressive/regex_actions.hpp>

#include "regexp.h"
#include "blocktable.h"
#include "timer.h"
#include "host.h"

using namespace boost::xpressive;
using namespace boost::property_tree;

using std::cerr;

class Iris
{
public:
    Iris();

    FILE *in;
    int main_loop();
};

#endif // IRIS_H
