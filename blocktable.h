#ifndef BLOCKTABLE_H
#define BLOCKTABLE_H

#ifdef DEBUG
#define D(x) x
#else
#define D(x)
#endif

#include <vector>
#include <map>

#include "timer.h"
#include "host.h"

class BlockTable
{
public:

    BlockTable();

    std::map<std::string, int> blktbl;
    std::vector<Host> hosts;

    // Methods for checking wether or not 
    // the host is blocked and to add/remove hosts
    // from the block table.
    //
    void block_host(std::string ipaddr, int blktime);
    void unblock_host(std::string ipaddr);
    bool is_blocked(std::string ipaddr_);
    bool is_registered(std::string ipaddr_);

    void register_host(Host host);
    void check();

};

#endif // BLOCKTABLE_H
