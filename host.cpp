#include "host.h"

Host::Host(ptree* prefs_, std::string ipaddr_)
{
    prefs  = prefs_;
    ipaddr = ipaddr_;
}

Host::~Host()
{
    delete prefs;
}

// Cheks the users vector to determian if the
// user has been tried already;
//
bool Host::has_user(std::string username)
{
    for(auto user : users)
    {
        if(user == username)
        {
            return true;
        }
    }
    return false;
}
