#include "iris.h"

Iris::Iris() { }

// This checks to see which hosts need to be removed from the blocktable
// 
void block_table_purge(BlockTable* tbl)
{
    for (;;)
    {
        D(cerr << "Iris: block_table_purge(): Sleeping..\n");
        tbl->check();
        std::this_thread::sleep_for(std::chrono::minutes(1));
    }
    return;
}


// The main loop which will loop every time we 
// receive a message from journalctl -f
// 
int Iris::main_loop()
{

    D(cerr << "Debuggin is on.\n");

    FILE *in;
    char buff[1024];

    Regexp* re = new Regexp();
    BlockTable* blktbl = new BlockTable(); 

    // Start the thread which purges the blocked hosts.
    //
    Timer(0, true, &block_table_purge, blktbl);

    if(!(in = popen("journalctl -f", "r")))
    {
        return 1;
    }

    while(fgets(buff, sizeof(buff), in) != NULL)
    {
        std::string msg = re->match_sshd_msg(buff);



        // Loop throught all th regex and try to determain
        // if the user has failed. Loop through all the failed ones
        // first, then determaie if it was successfull
        //
        for(sregex rgx : re->FAIL_REGEX)
        {
            smatch res = re->match_entry(rgx, msg);

            if(!res.empty())
            {
                if(!blktbl->is_registered(res["host"]))
                {
                    Host host(NULL, res["host"]);
                    blktbl->register_host(host);
                    D(cerr << "Regestering new host: " << res["host"] << "\n");
                }
                else
                {
                    D(cerr << "Login attempt from: " << res["host"] << "\n");
                }
            }
        }

        // If the regex is empty try match for the success auth
        // regex, if that is successfull then unblock the host / 
        // etc what ever the user has specfied in the config
        //
        smatch ressuc = re->match_entry(re->SUCCESS_AUTH, msg);
        if(!ressuc.empty())
        {
            D(cerr << "Host success: " << ressuc["user"] << "@" << ressuc["host"] << "\n");
            continue;
        }

    } // END_WHILE

    pclose(in);

    return 0;
}
