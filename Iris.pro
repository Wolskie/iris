#-------------------------------------------------
#
# Project created by QtCreator 2014-04-08T02:04:59
#
#-------------------------------------------------

QT       += core

QT       -= gui

QMAKE_CC = gcc
QMAKE_CXX = g++

QMAKE_CXXFLAGS += -std=c++11 -fpermissive -DDEBUG

TARGET = Iris
CONFIG   += console
CONFIG   -= app_bundle
CONFIG   += c++11

TEMPLATE = app


SOURCES += main.cpp \
    host.cpp \
    iris.cpp \
    regexp.cpp \
    blocktable.cpp \

HEADERS += \
    host.h \
    iris.h \
    regexp.h \
    blocktable.h \
    timer.h
